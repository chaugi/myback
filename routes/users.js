const express = require('express');
const router = express.Router();
const User = require('../model/Users');

//Listing all users
router.get('/', async (req, res) => {
    try {
        const users = await User.find().sort({_id:-1});
        res.json(users);
    } catch (err) {
        res.json({message: err})
    }
});

//Getting single user
router.get('/:userID', async (req, res) => {
    try {
        const user = await User.findById(req.params.userID);
        res.json(user);
    } catch (err) {
        res.json({message: err})
    }
});

//Deleting user
router.delete('/:userID', async (req, res) => {
    if(isValidApiKey(req, res) == false) return 0;
    try {
        const removedUser = await User.findByIdAndRemove(req.params.userID);
        res.json(removedUser);
    } catch (err) {
        res.json({message: err})
    }
});

//Updating user
router.patch('/:userID', async (req, res) => {
    if(isValidApiKey(req, res) == false) return 0;
    try {
        const updatedUser = await User.findByIdAndUpdate(
            req.params.userID,
            {$set: {surname: req.body.surname}}
        );
        res.json(updatedUser);
    } catch (err) {
        res.json({message: err})
    }
});

function isValidApiKey(req, res) {
    if(req.query.key != 'b56051db6') {
        res.status(403).json({message: "Wrong API key!"});
        return false;
    }
    return true;
}

//Adding new user
router.post('/', async (req, res) => {
    //console.log(req.body);
    if(isValidApiKey(req, res) == false) return 0;
    const user = new User({
        name: req.body.name,
        surname: req.body.surname,
        email: req.body.email
    });
    try {
        const savedUser = await user.save();
        res.status(200).json(savedUser);
    } catch(err) {
        res.status(400).json({message: err})
    }
});

module.exports = router;