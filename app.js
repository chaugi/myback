const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

//Import routes
const usersRoute = require('./routes/users');

//middlewares
app.use(cors());
app.use(bodyParser.json()); //converting incoming request body to json
app.use('/users', usersRoute); //mapping user routes to /users route

//Generic front page message
app.get('/', (req, res) => {
    res.send('Backend is working properly..');
});

//Connecting to DB
mongoose.connect(process.env.DB_CONNECTION,
    { useNewUrlParser: true },
    () => {
        console.log('Connected to db');
    }
);

//app listen the port
app.listen(3333);
